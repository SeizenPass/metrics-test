package main

import (
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net/http"
)

var (
	requestsErrorCount = promauto.NewCounter(prometheus.CounterOpts{
		Name: "requests_error_count",
		Help: "The error number of processed requests",
	})
	requestsTotalCount = promauto.NewCounter(prometheus.CounterOpts{
		Name: "requests_total_count",
		Help: "The total number of processed requests",
	})
)



func main() {
	http.Handle("/metrics", promhttp.Handler())
	http.Handle("/normal", &normal{})
	http.Handle("/errorPage", &errorPage{})
	fmt.Println("Starting server...")
	http.ListenAndServe(":2112", nil)
}

type normal struct {}

func (h *normal) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	requestsTotalCount.Inc()
	w.Write([]byte("Page is normal"))
}

type errorPage struct {}

func (h *errorPage) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	requestsErrorCount.Inc()
	requestsTotalCount.Inc()
	w.Write([]byte("error page!"))
}
